# crud-app

#### Projeto esqueleto para fins experimental usando Node, Express, GraphQL, MySQL e React


## Projeto contem 2 diretórios

**Diretório 1 - _Back-End:_ server**

> Onde fica o servidor da aplicação, desenvolvido usando:

- Node
- Expres
- GraphQL
- MySQL
- Apollo

**Rodando a applicação server**

> - Como pré-requisito você tem que ter instalado e configurado no seu computador o [GIT](http://bit.ly/2AZ5AeS) **_ultima versão estável do Node_** 
> de preferência a versão **_LTS_** [ACESSE](https://nodejs.org/en/download/).

> - #### Uma vez com tudo instaldo e configurado corretamente, vamos para os próximos passos >>>

> - Acessando no **_Mac/Linux/Windows_** o **_Terminal ou CMD_** navegue até uma pasta de sua preferência e execute: 
> `git clone git@gitlab.com:roniewill/crud-app.git`
> esse comando clona o projeto para sua maquina, feito isso vamos a seguinte.

> - Ainda no **_Terminal ou CMD_** navegue até o diretório onde você clonou o projeto **_crud-app/server_** e execute o **COMANDO:** 
> `npm install` e depois `npm start`
> se tudo ocorrer bem então vc verá a mensagem no **_Terminal ou CMD_**: `Running a GraphQL API server at localhost:4000/graphql`

> **OBS:** Existe uma configuração pra se conectar com o **DB**, então caso você instale o **MySQL** no sua maquina e 
> queira usar o DB local, então você precisa modificar o acesso em `server/index.js` na linha `64`

```
    host: 'dbhost',
    user: 'dbuser',
    password: 'dbpassword',
    database: 'dbname'
```    

**Diretório 2 - _Front-End:_ crud-app-client**

> Onde fica o client da aplicação, responsável pela interação com o usuário, desenvolvido usando:

- React
- [React-Router](http://bit.ly/2VurIal)
- [Bootstrap](http://bit.ly/2VrrXmN)
- [Formik](http://bit.ly/2M04Mwx)
- [GraphQL](https://graphql.org/)
- [Apollo](http://bit.ly/2M1nnss)

**Rodando a applicação crud-app-client**

> - Como pré-requisito você tem que ter instalado no seu computador a  **_ultima versão estável do Node_** como citado anteriormente
> de preferência a versão **_LTS_** [ACESSE](https://nodejs.org/en/download/).

> - Acessando no **_Mac/Linux/Windows_** o **_Terminal ou CMD_** navegue até o diretório **_crud-app/crud-app-client_** e execute o **COMANDO:** 
> `npm install` e depois `npm start`
> se tudo ocorrer bem então se abrirá uma nova aba no seu navegador(**Chrome, Firefox e etc...**) 
> e você deve ver a interface da aplicação.

> **OBS:** Existe uma configuração pra se conectar com o **servidor local ou externo**, caso queira usar o servidor local
> que possivelmente estará rodando em: `localhost:4000/graphql` então você precisa passar essa **URL** como parâmetro em:

```
crud-app-client/src/index.js
```
> e na linha `12` adicionar o novo endereço.

> Mesmo se você não modificar, a configuração estará apontando pra uma **URL** que deve funcionar da mesma forma

> **Obs:** fique atendo, pois os serviços externos sairão do ar

```
uri: 'localhost:4000/graphql',
```

#### Be Happy!!!
